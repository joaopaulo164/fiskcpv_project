from django.shortcuts import render

# Create your views here.

from django.contrib.auth.models import User
from rest_framework import serializers, viewsets

from .models import Song, Verse

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class VerseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Verse
        fields = (
            'url', 'order', 'sentence', 'start', 'startMilliseconds',
            'end', 'endMilliseconds', 'secret_word', 'word_options_array'
        )


class SongSerializer(serializers.ModelSerializer):
    verses = VerseSerializer(many=True, read_only=True)

    class Meta:
        model = Song
        fields = ('id', 'url', 'title', 'band', 'genre', 'embedded', 'duration', 'verses')

    # def verses(self):
    #     verses = Verse.objects.filter(pk=Song.id)
    #     return verses



# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SongViewSet(viewsets.ModelViewSet):
    queryset = Song.objects.all()
    serializer_class = SongSerializer


class VerseViewSet(viewsets.ModelViewSet):
    queryset = Verse.objects.all()
    serializer_class = VerseSerializer
