from django.contrib import admin
#from .models import (Song, Verse, Question, Answer)
from .models import (Song, Verse)

# Register your models here.

# (Inline - Dividido em blocos)
#
# class QuestionStackedInlineAdmin(admin.StackedInline):
#     model = Question
#
#
# class QuestionTabularInlineAdmin(admin.TabularInline):
#     model = Question
#

class VerseAdmin(admin.ModelAdmin):

    list_display = ['song_title', 'order', 'sentence', 'start', 'end', 'secret_word']
    search_fields = ['sentence']
    list_filter = ['song__title']

    #
    # inlines = [
    #     QuestionTabularInlineAdmin, # inline
    # ]



    def song_title(self, obj):
        return obj.song.title
    song_title.short_description = 'Música'



class VerseStackedInlineAdmin(admin.StackedInline):
    model = Verse
    extra = 0


class SongAdmin(admin.ModelAdmin):
    list_display = ['title', 'band', 'genre', 'duration']
    search_fields = ['title', 'band', 'genre']
    list_filter = ['title', 'band', 'genre']

    inlines = [
        VerseStackedInlineAdmin, # inline
    ]



admin.site.register(Song, SongAdmin)
#admin.site.register(Verse, VerseAdmin)
#admin.site.register(WordOption)
#admin.site.register(Question)
#admin.site.register(Answer)
