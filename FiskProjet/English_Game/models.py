# -*- coding: utf-8 -*-
from django.core.validators import MaxValueValidator

from django.db import models
import datetime



# Create your models here.


# level
# verses


class Song(models.Model):
    title = models.CharField('Título', max_length=100)
    band = models.CharField('Banda', max_length=100)
    genre = models.CharField('Gênero', max_length=50)
    #slug = models.SlugField('Atalho')
    embedded = models.TextField('Vídeo embedded', blank=True)
    duration = models.DurationField('Duração', default=datetime.timedelta(hours=0, minutes=0, seconds=0), editable=True)

    created_at = models.DateTimeField('Criado em', auto_now_add=True)  # auto_now_add = preenche com a data de criação
    updated_at = models.DateTimeField('Atualizado em',
                                      auto_now=True)  # auto_now = preenche com a data atual toda vez que for salvo (atualizado)

    def is_embedded(self):
        return bool(self.embedded)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Música'
        verbose_name_plural = 'Músicas'


# sentence
# time
# position_question
# options_answer


# class WordOption(models.Model):
#     word = models.CharField("Palavra", max_length=50, blank=True)
#
#
#     def __str__(self):
#         return self.word
#
#     class Meta:
#         verbose_name = 'Opção de palavra'
#         verbose_name_plural = 'Opções de palavras'


class Verse(models.Model):

    song = models.ForeignKey(Song, verbose_name='Música', related_name='verses')
    order = models.IntegerField('Order')
    sentence = models.CharField('Sentença', max_length=300)
    start = models.TimeField('Início', default=datetime.timedelta(hours=0, minutes=0, seconds=0, microseconds=0))
    startMilliseconds = models.IntegerField('Início - Milisegundos', default=0, validators=[MaxValueValidator(999)])
    end = models.TimeField('Fim', default=datetime.timedelta(hours=0, minutes=0, seconds=0, microseconds=0))
    endMilliseconds = models.IntegerField('Fim - Milisegundos', default=0, validators=[MaxValueValidator(999)])
    secret_word = models.CharField('Palavra secreta', max_length=50)
    word_options = models.TextField('Palavras separadas apenas por espaço:')


    def __str__(self):
        return self.song.title

    def word_options_array(self):
        return self.word_options.split(' ')

    class Meta:
        verbose_name = 'Verso'
        verbose_name_plural = 'Versos'
        ordering = ('order',)


#
# class Question(models.Model):
#     description = models.CharField('Sentença', max_length=50)
#     verse = models.ForeignKey(Verse, verbose_name='Verso', related_name='questions')
#
#     created_at = models.DateTimeField('Criado em', auto_now_add=True)  # auto_now_add = preenche com a data de criação
#     updated_at = models.DateTimeField('Atualizado em',
#                                       auto_now=True)  # auto_now = preenche com a data atual toda vez que for salvo (atualizado)
#
#     def __str__(self):
#         return self.description
#     class Meta:
#         verbose_name = 'Questão'
#         verbose_name_plural = 'Questões'
#
#
# class Answer(models.Model):
#     description = models.CharField('Sentença', max_length=50)
#     question = models.ForeignKey(Verse, verbose_name='Questão', related_name='answers')
#
#     created_at = models.DateTimeField('Criado em', auto_now_add=True)  # auto_now_add = preenche com a data de criação
#     updated_at = models.DateTimeField('Atualizado em',
#                                       auto_now=True)  # auto_now = preenche com a data atual toda vez que for salvo (atualizado)
#
#     def __str__(self):
#         return self.description
#
#     class Meta:
#         verbose_name = 'Resposta'
#         verbose_name_plural = 'Respostas'
#
