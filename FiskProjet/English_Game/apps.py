from django.apps import AppConfig


class EnglishGameConfig(AppConfig):
    name = 'FiskProjet.English_Game'
